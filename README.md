# Movie Theater Frontend

The frontend for the Movie Theater App

## Stack & Framework

The app is developed using next.js

## Pages

- index page is the main discover page
- /manage will open the admin dashboard
- /[movieId] is the movie details for a specific movie

## Get started

```
npm run dev
```
