import { useRouter } from "next/router";
import styles from "../../styles/MovieDetailsPage.module.css";
import "react-toastify/dist/ReactToastify.css";
import BookingForm from "../../components/BookingForm";
import React, { useState, useEffect } from "react";
import axios from "axios";
import MovieDetails from "../../components/MovieDetails";
import ReviewList from "../../components/ReviewList";
export default function () {
  const router = useRouter();
  const { movieId } = router.query;

  const [movie, setMovie] = useState({});
  const [reviews, setReviews] = useState([]);

  const getMovieRequest = async () => {
    if (!movieId) {
      return;
    }

    const url = `http://localhost:8080/api/v1/movies/${movieId}`;
    const reviewsUrl = `http://localhost:8080/api/v1/movies/${movieId}/reviews`;
    const movieDetailsResponse = await axios.get(url);
    const reviewsResponse = await axios.get(reviewsUrl);
    setMovie(movieDetailsResponse.data);
    setReviews(reviewsResponse.data);
  };

  useEffect(() => {
    getMovieRequest();
  }, [movieId]);
  return (
    <div className={styles.container}>
      <h1>{movie.title}</h1>
      <MovieDetails movie={movie} shouldHideButton={true}></MovieDetails>
      <BookingForm movie={movie}></BookingForm>
      <ReviewList reviews={reviews}></ReviewList>
    </div>
  );
}
