import SearchBox from "../components/SearchBox";
import React, { useState, useEffect } from "react";
import axios from "axios";
import BookingList from "../components/BookingList";
export default function () {
  const [bookings, setBookings] = useState(null);
  const [searchValue, setSearchValue] = useState("");

  const getBookingsRequest = async (searchValue) => {
    const url = searchValue
      ? `http://localhost:8080/api/v1/admin/bookings/search?query=${searchValue}`
      : `http://localhost:8080/api/v1/admin/bookings`;

    const response = await axios.get(url);

    setBookings(response.data);
  };

  useEffect(() => {
    getBookingsRequest(searchValue);
  }, [searchValue]);

  return (
    <div>
      <h1>Admin Dashboard</h1>
      <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
      <BookingList bookings={bookings || []}></BookingList>
    </div>
  );
}
