import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import MovieList from "../components/MovieList";
import SearchBox from "../components/SearchBox";

import { useState, useEffect } from "react";
import { PageHeader } from "react-bootstrap";
const axios = require("axios").default;
export default function Home() {
  const [movies, setMovies] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const getMovieRequest = async (searchValue) => {
    const url = searchValue
      ? `http://localhost:8080/api/v1/movies/search?query=${searchValue}`
      : `http://localhost:8080/api/v1/movies`;

    const response = await axios.get(url);

    setMovies(response.data);
  };

  useEffect(() => {
    getMovieRequest(searchValue);
  }, [searchValue]);
  return (
    <div className={styles.container}>
      <Head>
        <title>AlbathaNext Movies</title>
        <meta name="description" content="Book with us the best movies" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>Watch the latest movies with us</h1>
      <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
      <main className={styles.main}>
        <MovieList movies={movies} />
      </main>
    </div>
  );
}
