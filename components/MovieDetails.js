import React from "react";

import styles from "../styles/MovieItem.module.css";

import Card from "react-bootstrap/Card";
import Genres from "./Genres";

export default function MovieDetails(props) {
  const imagesPath = "https://image.tmdb.org/t/p/w200";
  return (
    <div className={styles.movie}>
      <Card style={{ width: "35rem" }}>
        <Card.Img variant="top" src={imagesPath + props.movie.poster_path} />
        <Card.Body>
          <Card.Title>{props.movie.title}</Card.Title>
          <Card.Text>{props.movie.overview}</Card.Text>
          <Card.Text>{props.movie.vote_average}</Card.Text>
          <Card.Text>{props.movie.vote_count} people voted</Card.Text>
          <Card.Text>release date: {props.movie.release_date}</Card.Text>
        </Card.Body>
      </Card>

      <Genres genres={props.movie.genres || []}></Genres>
    </div>
  );
}
