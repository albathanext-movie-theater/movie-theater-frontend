import React, { useState, useRef } from "react";
import Button from "react-bootstrap/Button";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Form from "react-bootstrap/Form";
import axios from "axios";
import styles from "../styles/BookingForm.module.css";
import Datetime from "react-datetime";

export default function BookingForm(props) {
  const notifySuccessfulBooking = () => toast("Booking is successful");

  const [date, setDate] = useState(new Date());
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [numberOfSeats, setNumberOfSeats] = useState(1);

  const clearInput = () => {
    setFirstName("");
    setLastName("");
    setNumberOfSeats(1);
    setEmail("");
  };
  const submitBooking = async (event) => {
    event.preventDefault();
    const data = {
      email,
      firstName,
      lastName,
      movieId: props.movie.id,
      showTimeAt: date.getTime(),
      bookedAt: +new Date(),
      numberOfSeats,
    };

    try {
      await axios.post("http://localhost:8080/api/v1/bookings", data);
    } catch (error) {
      if (error.response.status === 429) {
        alert("You are only allowed 10 tickets per movie");
        return;
      }
    }
    clearInput();
    alert("Booking is successful");
    notifySuccessfulBooking();
  };

  return (
    <div>
      <Form onSubmit={submitBooking} className={styles.form}>
        <Form.Group className="mb-3" controlId="date">
          <Form.Label>Pick a date</Form.Label>
          <Datetime
            value={date}
            onChange={(datePicked) => setDate(datePicked.toDate())}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            required
            value={email}
            onInput={(e) => setEmail(e.target.value)}
            type="email"
            placeholder="Enter email"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="firstname">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            required
            value={firstName}
            onInput={(e) => setFirstName(e.target.value)}
            type="text"
            placeholder="Enter first name"
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="lastname">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            required
            value={lastName}
            onInput={(e) => setLastName(e.target.value)}
            type="text"
            placeholder="Enter last name"
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="seat">
          <Form.Label>Number of seats</Form.Label>
          <Form.Control
            required
            value={numberOfSeats}
            onInput={(e) => setNumberOfSeats(e.target.value)}
            type="text"
            placeholder="Enter number of seats"
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Book
        </Button>
      </Form>
    </div>
  );
}
