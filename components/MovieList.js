import React from "react";
import MovieItem from "./MovieItem";
import styles from "../styles/MovieList.module.css";
import Link from "next/link";
export default function MovieList(props) {
  return (
    <div
      className={styles.list}
      className="d-flex align-content-start flex-wrap"
    >
      {props.movies.map((movie, index) => (
        <Link href={`/movie/${movie.id}`}>
          <MovieItem movie={movie} key={movie.id}></MovieItem>
        </Link>
      ))}
    </div>
  );
}
