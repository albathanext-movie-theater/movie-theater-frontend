import React from "react";

import Badge from "react-bootstrap/Badge";

export default function Genres(props) {
  return (
    <div>
      {props.genres.map((genre, index) => (
        <h6 key={genre.id}>
          <Badge bg="secondary">{genre.name}</Badge>
        </h6>
      ))}
    </div>
  );
}
