import React from "react";
import axios from "axios";
import styles from "../styles/BookingItem.module.css";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

export default function BookingItem(props) {
  const cancelBooking = async () => {
    await axios.patch(
      `http://localhost:8080/api/v1/admin/bookings/cancel/${props.booking.id}`
    );
  };

  return (
    <div className={styles.card}>
      <Table>
        <tbody>
          <tr>
            <td>Email</td>
            <td>{props.booking.email}</td>
          </tr>
          <tr>
            <td>First name</td>
            <td>{props.booking.firstName}</td>
          </tr>
          <tr>
            <td>Last name</td>
            <td>{props.booking.lastName}</td>
          </tr>
          <tr>
            <td>Show time</td>
            <td>{new Date(props.booking.showTimeAt).toString()}</td>
          </tr>
          <tr>
            <td>Booked at</td>
            <td>{new Date(props.booking.bookedAt).toString()}</td>
          </tr>
          <tr>
            <td>seats number</td>
            <td>{props.booking.numberOfSeats}</td>
          </tr>
          <tr>
            <td>status</td>
            <td>{props.booking.active ? "Active" : "Cancelled"}</td>
          </tr>
        </tbody>
      </Table>

      <Button onClick={cancelBooking}>Cancel</Button>
    </div>
  );
}
