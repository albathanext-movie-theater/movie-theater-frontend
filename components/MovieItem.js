import React from "react";

import styles from "../styles/MovieItem.module.css";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

export default function MovieItem(props) {
  const imagesPath = "https://image.tmdb.org/t/p/w500";
  return (
    <div className={styles.movie}>
      <Card style={{ width: "14rem" }}>
        <Card.Img variant="top" src={imagesPath + props.movie.poster_path} />
        <Card.Body>
          <Card.Title>{props.movie.title}</Card.Title>
          <Card.Text>{props.movie.vote_average}</Card.Text>
          <Card.Text>{props.movie.vote_count} people voted</Card.Text>
          <Card.Text>release date: {props.movie.release_date}</Card.Text>
          {!props.shouldHideButton && (
            <Button variant="primary" href={`/movie/${props.movie.id}`}>
              Check me out
            </Button>
          )}
        </Card.Body>
      </Card>
    </div>
  );
}
