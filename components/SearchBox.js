import React from "react";
import styles from "../styles/SearchBox.module.css";
export default function SearchBox(props) {
  return (
    <div className="col col-sm-4" className={styles.box}>
      <input
        className="form-control"
        value={props.value}
        onChange={(event) => props.setSearchValue(event.target.value)}
        placeholder="Type to search..."
      ></input>
    </div>
  );
}
