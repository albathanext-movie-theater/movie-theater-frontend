import React from "react";
import BookingItem from "./BookingItem";

export default function BookingList(props) {
  return (
    <div>
      {props.bookings.map((booking, index) => (
        <BookingItem booking={booking} key={booking.id}></BookingItem>
      ))}
    </div>
  );
}
