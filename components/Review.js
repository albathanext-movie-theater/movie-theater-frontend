import React from "react";
import Accordion from "react-bootstrap/Accordion";
import Badge from "react-bootstrap/Badge";
import styles from "../styles/Review.module.css";
export default function Review(props) {
  return (
    <div className={styles.review}>
      <Accordion>
        <Accordion.Item eventKey={props.review.id}>
          <Accordion.Header>
            By {props.review.author}
            <h6 style={{ position: "absolute", right: "50px" }}>
              <Badge bg="secondary">{props.review.updated_at}</Badge>
            </h6>
          </Accordion.Header>
          <Accordion.Body>{props.review.content}</Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
}
