import React from "react";

import styles from "../styles/MovieList.module.css";

import Review from "./Review";
export default function ReviewList(props) {
  return (
    <div className={styles.list}>
      <h2>Reviews</h2>
      {props.reviews.map((review, index) => (
        <Review review={review} key={review.id}></Review>
      ))}
    </div>
  );
}
